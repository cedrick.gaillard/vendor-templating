# mass-update

## Getting started

Install dependencies:

    $ npm install

Fill a token.txt file with your bearer token inside.

## Purpose

This script will copy the vendor configuration from a template notice and will set the same information for all other notices in the organization.

## TODO

When a notice has been updated, you have to go in the console and manually save & publish the configuration.

## Usage

Set the organization id and the notice you want to use as a template

    $ node index.js --org <org_id> --notice <notice_id>


## Example

    $ node index.js --org cedrick-sandbox --notice YyjiFyzt 
    Getting the list of notices for organization: cedrick-sandbox
    Getting the versions of notices for : YyjiFyzt
    Patching notice: Mg4PktWN
    Patching notice: pk8pzUQ3
    Patching notice: ZNYNKZ9h

