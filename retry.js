const retry = require('retry');
const { merge } = require('lodash');

const defaultOptions = {
  retries: 3, // Retry three times
  minTimeout: 1000, // Wait at least 1 second before retrying the first time
  maxTimeout: 20 * 1000, // Wait for max 20 seconds
  randomize: true, // Randomizes the timeouts by multiplying with a factor between 1 to 2
};

/**
 * Retry an operation using exponential back-off until it returns successfully.
 * @param action
 * @param retryOptions
 * @returns {Promise<unknown>}
 */
function retryable(action, retryOptions) {
  const options = merge({}, defaultOptions, retryOptions);
  const operation = retry.operation(options);

  return new Promise((resolve, reject) => {
    operation.attempt(async () => {
      try {
        //console.log(action.toString())
        const result = await action();
        resolve(result);
      } catch (err) {
        if (!operation.retry(err)) {
          reject(err);
        }
      }
    });
  });
}

module.exports = {
  retryable,
};

