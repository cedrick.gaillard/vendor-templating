const bent = require('bent')
const fs = require('fs')
const process = require('process')
const { retryable } = require('./retry');

'use strict';

try {
    try_token = fs.readFileSync("token.txt", 'utf8').trim()
} catch (err) {
    console.error(err)
}

function display_usage() {
    console.log("Usage:")
    console.log("Fill a token.txt file with your bearer token inside then you can run this program using the args:")
    console.log("node index.js --org <orgid> --notice <noticeid>")
}

var args = process.argv.slice(2);
let orga, notice
args.forEach(function (val, idx, arr) {
    if (val == "--org") { orga = arr[idx+1]; }
    if (val == "--notice") { notice = arr[idx+1]; }
    if (val == "--help" || val == "-h") {
        display_usage()
        process.exit(0)
    }
});

if (! orga) {
    console.log("Missing organization id")
    display_usage()
    process.exit(1);
}
if (! notice) {
    console.log("Missing notice template")
    display_usage()
    process.exit(1);
}

const config = {
    /**
     * Organization ID
     */
    organizationId: orga,

    /**
     * Notice ID to use as template
     */
    noticeIdTpl: notice,

    /**
     * Bearer Token
     */
    token: try_token,

    /**
     * Default limit per request
     */
    limit: 100
};

const client = {
    get: bent('GET', 'https://api.didomi.io/v1', 'json', {
        Authorization: `Bearer ${config.token}`,
    }),
    post: bent('POST', 'https://api.didomi.io/v1', 'json', {
        Authorization: `Bearer ${config.token}`,
    }, 201),
    remove: bent('DELETE', 'https://api.didomi.io/v1', 'json', {
        Authorization: `Bearer ${config.token}`,
    }),
    patch: bent('PATCH', 'https://api.didomi.io/v1', 'json', {
        Authorization: `Bearer ${config.token}`,
    }),
};

(async () => {
    console.log("Getting the list of notices for organization: " + config.organizationId)

    let list_of_notices;
    try {
        list_of_notices = await retryable(
            () => client.get(`/widgets/notices?organization_id=${config.organizationId}&$limit=${config.limit}`), {retries: 3, minTimeout: 1000, maxTimeout: 2000}
        );
    }
    catch(error) {
        console.log(error);
        process.exit(1);
    }

    // To retrieve more than XXX notices
    const nb_notices = list_of_notices.total
    if (nb_notices > config.limit) {
        var retrieved = list_of_notices.data.length
        while (retrieved < nb_notices) {
            notices = await retryable(
                () => client.get(`/widgets/notices?organization_id=${config.organizationId}&$limit=${config.limit}&$skip=${retrieved}`)
            );
            for (const value of notices.data) {
                list_of_notices.data.push(value); 
            }
            retrieved += notices.data.length;
        }
    }
    
    // Get the vendors of the tpl notice
    console.log("Getting the versions of notices for : " + config.noticeIdTpl)
    const notices_from_tpl = await retryable(
        () => client.get(`/widgets/notices/configs?organization_id=${config.organizationId}&notice_id=${config.noticeIdTpl}`)
    );

    // To retrieve more than XXX versions
    const nb_versions = notices_from_tpl.total
    if (nb_versions > config.limit) {
        var retrieved = notices_from_tpl.length
        while (retrieved < nb_versions) {
            versions = await retryable(
                () => client.get(`/widgets/notices/configs?organization_id=${config.organizationId}&notice_id=${config.noticeIdTpl}&$skip=${retrieved}`)
            );
            for (const value of versions.data) {
                notices_from_tpl.data.push(value);
            }
            retrieved += notices_from_tpl.data.length
        }
    }

    list_of_vendors = ""
    for (const value of notices_from_tpl.data) {
        if (!value.deployed_at) {
            list_of_vendors = value.config.app.vendors
        }
    }

    for (const value of list_of_notices.data) {
        if (value.id != config.noticeIdTpl) {
            var notice = await retryable(
                () => client.get(`/widgets/notices/configs?organization_id=${config.organizationId}&notice_id=${value.id}`)
            );

            // To retrieve more than XXX versions
            const nb_versions = notice.total
            if (nb_versions > config.limit) {
                var retrieved = notices_from_tpl.length
                while (retrieved < nb_versions) {
                    versions = await retryable(
                        () => client.get(`/widgets/notices/configs?organization_id=${config.organizationId}&notice_id=${value.id}&$skip=${retrieved}`)
                    );
                    for (const versionvalue of versions.data) {
                        notice.data.push(versionvalue);
                    }
                    retrieved += notice.data.length
                }
            }

            for (const version of notice.data) {
                if (!version.deployed_at) {
                    version.config.app.vendors = list_of_vendors
                    version_id = version.id
                    console.log("Patching notice: " + version_id)
                    try {
                        temp = await retryable(
                            () => client.patch(`/widgets/notices/configs/${version.id}`, version)
                        );
                    }
                    catch(error) {
                        console.log(error);
                    }
                }
            }
        }
    }

})();